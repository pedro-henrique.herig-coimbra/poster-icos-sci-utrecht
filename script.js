function next() {
      var qElems = document.querySelectorAll('#questions>div');
      for (var i = 0; i < qElems.length; i++) {
          if (qElems[i].style.display != 'none') {
              qElems[i].style.display = 'none';
              if (i == qElems.length - 1) {
                  qElems[0].style.display = 'block';
              } else {
                  qElems[i + 1].style.display = 'block';
              }
              break;
          }
      }
  }
  function prev() {
      var qElems = document.querySelectorAll('#questions>div');
      for (var i = 0; i < qElems.length; i++) {
          if (qElems[i].style.display != 'none') {
              qElems[i].style.display = 'none';
              if (i == 0) {
                  qElems[qElems.length - 1].style.display = 'block';
              } else {
                  qElems[i-1].style.display = 'block';
              }
              break;
          }
      }
  }
  window.onkeydown = function(event) {
      // https://www.toptal.com/developers/keycode/for/arrow-right%
      if (event.keyCode == 39) {
          next()
      };
      if (event.keyCode == 37) {
          prev()
      };
  };

  document.addEventListener('mousedown', lock, false);
  document.addEventListener('touchstart', lock, false);

  document.addEventListener('mouseup', move, false);
  document.addEventListener('touchend', move, false);

  let x0 = null
  let startY = null
  let startTime = null;

  function lock(e) {
    swipedir = 'none';
    dist = 0;
    x0 = e.pageX;
    y0 = e.pageY;
    startTime = new Date().getTime(); // record time when finger first makes contact with surface
  };

  function move(e) {

    if(x0 || x0 === 0) {
      let dx = e.pageX - x0, s = Math.sign(dx);
      let elapsedTime = new Date().getTime() - startTime; // get time elapsed

      if(Math.abs(dx)>50 && elapsedTime<300){
        if(s>0) {
          next()
        } else {
          prev()
        };
      }

      x0 = null
      y0 = null
      startTime = null
    }
  };
