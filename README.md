# poster ICOS Sci Utrecht

Project dedicated to the poster for ICOS Science Conference 12-15/09/2022 in Utrecht (Netherlands).

Poster page: https://pedro-henrique.herig-coimbra.pages.mia.inra.fr/poster-icos-sci-utrecht/index.html

<img src="presentation/images/static/pageposter.png" alt="page" style="width:50%;">
